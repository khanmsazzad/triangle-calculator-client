package org.sazzad.triangle.Client;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import junit.framework.TestCase;

import org.junit.Test;

import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class CommunicatorTest extends TestCase {

	@Test
	public void testCommunicatorInstance() throws Exception {
		ConnectionFactory mockConnectionFactory = mock(ConnectionFactory.class);
		Connection mockConnection = mock(Connection.class);
		Channel mockChannel = mock(Channel.class);
		DeclareOk mockDeclareOk = mock(DeclareOk.class);

		when(mockConnectionFactory.newConnection()).thenReturn(mockConnection);
		when(mockConnection.createChannel()).thenReturn(mockChannel);
		when(mockChannel.queueDeclare()).thenReturn(mockDeclareOk);
		when(mockDeclareOk.getQueue()).thenReturn("rpc_queue");

		Communicator communicator = new Communicator(mockConnectionFactory);
		assertNotNull("Not null Instance expected ", communicator);
		communicator = new Communicator(mockConnectionFactory, true);
		assertNotNull("Not null instance expected", communicator);
		try {
			communicator.establishCommunication();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception is not expected");
		} finally {
			try {
				communicator.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testEstablishCommunicationFailure() throws Exception {
		ConnectionFactory mockConnectionFactory = mock(ConnectionFactory.class);
		Connection mockConnection = mock(Connection.class);
		Channel mockChannel = mock(Channel.class);
		DeclareOk mockDeclareOk = mock(DeclareOk.class);

		when(mockConnectionFactory.newConnection()).thenReturn(mockConnection);
		when(mockChannel.queueDeclare()).thenReturn(mockDeclareOk);
		when(mockDeclareOk.getQueue()).thenReturn("rpc_queue");

		Communicator communicator = new Communicator(mockConnectionFactory,
				true);
		assertNotNull("Not null instance expected", communicator);

		boolean exceptionOccured = false;
		try {
			communicator.establishCommunication();
		} catch (Exception e) {
			exceptionOccured = true;
		} finally {
			if (!exceptionOccured) {
				fail("exception expected");
			}
			try {
				communicator.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		communicator = new Communicator(mockConnectionFactory, true);
		assertNotNull("Not null instance expected", communicator);

		when(mockConnection.createChannel()).thenReturn(mockChannel);

		exceptionOccured = false;
		try {
			communicator.establishCommunication();
			communicator.call(MessageCreator.createMessage(new float[] { 30,
					60, 90, 0 }, "-angle"));
		} catch (Exception e) {
			exceptionOccured = true;
		} finally {
			if (!exceptionOccured) {
				fail("exception expected");
			}
			try {
				communicator.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testCommunicatorCall() throws Exception {
		ConnectionFactory mockConnectionFactory = mock(ConnectionFactory.class);
		Channel mockChannel = mock(Channel.class);

		byte[] messageAngle = MessageCreator.createMessage(new float[] { 30,
				60, 90 }, "-angle");
		byte[] messageSide = MessageCreator.createMessage(new float[] { 5, 12,
				13 }, "-side");
		TriangleProtos.Triangle responseAngle = TriangleProtos.Triangle
				.parseFrom(messageAngle);
		TriangleProtos.Triangle responseSide = TriangleProtos.Triangle
				.parseFrom(messageSide);

		Communicator communicator = new Communicator(mockConnectionFactory,
				true, responseAngle, mockChannel);
		try {
			communicator.call(messageAngle);
		} catch (Exception e) {
			fail("Exception is not expected");
		}

		communicator = new Communicator(mockConnectionFactory, true,
				responseSide, mockChannel);
		try {
			communicator.call(messageSide);
		} catch (Exception e) {
			fail("Exception is not expected");
		}
	}
}
