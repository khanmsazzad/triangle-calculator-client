package org.sazzad.triangle.Client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.easymock.EasyMock;
import org.junit.Test;

public class AppTest {

	@Test
	public void testInstanceCreate() {
		App app = new App();
		assertNotNull("Not null Instance expected", app);
	}

	@Test
	public void testHasValidSides() {
		assertTrue("5 12 13 valid side of a triangle : ",
				App.hasValidSides(new float[] { 5, 12, 13 }));
		assertFalse("0 12 12 not a valid side of a triangle ",
				App.hasValidSides(new float[] { 0, 12, 12 }));
	}

	@Test
	public void testHasValidAngles() {
		assertTrue("30 60 90 valid angles of a trinagle ",
				App.hasValidAngles(new float[] { 30, 60, 90 }));
		assertFalse("30 60 100 doesn't form a triangle ",
				App.hasValidAngles(new float[] { 30, 60, 100 }));
	}

	@Test
	public void testHasValidInput() {
		illegalArgumentExp(new String[] { "-side", "5", "-side", "12", "-side",
				"13" }, "-anyvalue", "-anyvalue is not valid");
		illegalArgumentExp(new String[] { "-side", "5", "-side", "12", "-side",
				"13" }, "-angle", "-angle is not valid");
		illegalArgumentExp(new String[] { "-angle", "30", "-angle", "60",
				"-angle", "90" }, "-side", "-side is not valid");
		illegalArgumentExp(new String[] { "-angle", "30", "-angle", "60",
				"-side", "90" }, "-angle", "-input format is not write");

		assertTrue(
				"valid side",
				App.hasValidInput(new String[] { "-side", "5", "-side", "12",
						"-side", "13" }, "-side"));
		assertTrue(
				"valid angle",
				App.hasValidInput(new String[] { "-angle", "30", "-angle",
						"60", "-angle", "90" }, "-angle"));

		boolean numberformatExp = false;
		try {
			App.hasValidInput(new String[] { "-side", "5", "-side", "12",
					"-side", "13abc" }, "-side");
		} catch (NumberFormatException nme) {
			numberformatExp = true;
		} finally {
			if (!numberformatExp) {
				fail("NumberFormatException expected");
			}
		}
	}

	private void illegalArgumentExp(String[] input, String paramType,
			String message) {
		boolean illegalArg = false;
		try {
			App.hasValidInput(input, paramType);
		} catch (IllegalArgumentException irg) {
			illegalArg = true;
		} finally {
			if (!illegalArg) {
				fail(message);
			}
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExceptionForNegative() {
		String[] args = { "-angle", "-10", "-angle", "-10", "-angle", "-10" };
		App.main(args);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testExceptionMixedInput() {
		String[] args = { "-side", "-10", "-angle", "-10", "-angle", "-10" };
		App.main(args);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExceptionForZero() {
		String[] args = { "-side", "0", "-angle", "-10", "-angle", "-10" };
		App.main(args);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExceptionForInvalid() {
		String[] args = { "-side", "5", "-side", "5", "-side", "10" };
		App.main(args);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testExceptionLessArgs() {
		String[] args = { "-side", "27B" };
		App.main(args);
	}

	@Test
	public void testValidInput() {
		Communicator testComm = EasyMock.createMock(Communicator.class);
		try {
			testComm.call((byte[]) EasyMock.anyObject());
		} catch (Exception e) {
			e.printStackTrace();
		}
		EasyMock.expectLastCall();
		EasyMock.replay(testComm);
		EasyMock.verify();
	}

}
