package org.sazzad.triangle.Client;

import junit.framework.TestCase;

import org.junit.Test;

public class MessageCreatorTest extends TestCase {

	@Test
	public void testCreateMessgae() {
		MessageCreator messageCreator = new MessageCreator();
		assertNotNull("Not null instance created", messageCreator);
		byte[] bytes = MessageCreator.createMessage(new float[] { 30, 60, 90 },
				"-angle");
		assertNotNull("Not null byte expected", bytes);
	}
}
