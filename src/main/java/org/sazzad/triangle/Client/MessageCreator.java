package org.sazzad.triangle.Client;

import org.sazzad.triangle.Client.TriangleProtos.Triangle;

public class MessageCreator {
	public static byte[] createMessage(float[] parameters, String paramType) {
		Triangle input = Triangle.newBuilder().setParameterType(paramType)
				.setParamA(parameters[0]).setParamB(parameters[1])
				.setParamC(parameters[2]).build();
		return input.toByteArray();

	}
}
