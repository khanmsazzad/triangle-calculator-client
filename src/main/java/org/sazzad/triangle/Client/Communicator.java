package org.sazzad.triangle.Client;

import java.util.UUID;

import org.sazzad.triangle.Client.TriangleProtos.Triangle;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class Communicator {
	private ConnectionFactory factory;
	private Connection connection;
	private Channel channel;
	private String requestQueueName = "rpc_queue";
	private String replyQueueName;
	private QueueingConsumer consumer;
	private boolean isUnitTestRunning;
	private Triangle response = null;

	public Communicator(ConnectionFactory pConnectionFactory) {
		factory = pConnectionFactory;
		isUnitTestRunning = false;
	}

	public Communicator(ConnectionFactory pConnectionFactory,
			boolean pPIsUnitTestRunning) {
		factory = pConnectionFactory;
		isUnitTestRunning = pPIsUnitTestRunning;
	}

	public Communicator(ConnectionFactory pConnectionFactory,
			boolean pPIsUnitTestRunning, Triangle pResponse, Channel pChannel) {
		factory = pConnectionFactory;
		isUnitTestRunning = pPIsUnitTestRunning;
		response = pResponse;
		channel = pChannel;
	}

	public void establishCommunication() {

		try {
			factory.setHost("localhost");
			connection = factory.newConnection();
			channel = connection.createChannel();

			replyQueueName = channel.queueDeclare().getQueue();
			consumer = new QueueingConsumer(channel);
			channel.basicConsume(replyQueueName, true, consumer);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(e);
		} finally {
			if (isUnitTestRunning) {
				try {
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}

	}

	public void call(byte[] message) throws Exception {
		String corrId = UUID.randomUUID().toString();
		// System.out.println(corrId);
		BasicProperties props = new BasicProperties.Builder()
				.correlationId(corrId).replyTo(replyQueueName).build();

		channel.basicPublish("", requestQueueName, props, message);

		while (true && !isUnitTestRunning) {
			QueueingConsumer.Delivery delivery = consumer.nextDelivery();
			if (delivery.getProperties().getCorrelationId().equals(corrId)) {
				response = Triangle.parseFrom(delivery.getBody());
				break;
			}
		}

		String paramType = response.getParameterType().toLowerCase();
		if (paramType.equals("-angle")) {
			System.out
					.println("Area and perimeter calculation is not possible");

		} else if (paramType.equals("-side")) {
			System.out.println("Angle A: " + response.getParamA()
					+ " Angle B: " + response.getParamB() + " Angle C: "
					+ response.getParamC());
			System.out.println("Area: " + response.getArea() + " Perimeter: "
					+ response.getPerimeter());
		}
	}

	public void close() throws Exception {
		connection.close();
	}
}
