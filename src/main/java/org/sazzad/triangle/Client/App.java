package org.sazzad.triangle.Client;

import com.rabbitmq.client.ConnectionFactory;

public class App {
	private static float[] parameters = new float[3];

	public static void main(String[] argv) {
		Communicator triangleCalc = null;
		String paramType = argv[0].toLowerCase();

		if (argv.length == 6) {

			byte[] input = null;
			if (hasValidInput(argv, paramType)) {
				if ((hasValidAngles(parameters) && paramType.equals("-angle"))
						|| (hasValidSides(parameters) && paramType
								.equals("-side"))) {
					if (paramType.equals("-angle")) {
						System.out
								.println("Area and perimeter calculation is not possible");
						System.exit(0);
					}
					input = MessageCreator.createMessage(parameters, paramType);
					try {
						ConnectionFactory connectionFactory = new ConnectionFactory();
						triangleCalc = new Communicator(connectionFactory);
						triangleCalc.establishCommunication();
						triangleCalc.call(input);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						if (triangleCalc != null) {
							try {
								triangleCalc.close();
							} catch (Exception ignore) {
							}
						}
					}
				} else {
					throw new IllegalArgumentException(
							"Not a possible triangle");
				}
			}

		} else {
			throw new IllegalArgumentException(
					"Input Format: 1. -side x -side y -side \n 2. -angle x -angle y -angle z");
		}

	}

	public static boolean hasValidInput(String[] input, String parameterType) {
		if (parameterType.toLowerCase().equals("-side")
				|| parameterType.toLowerCase().equals("-angle")) {
			for (int i = 2; i < 6; i += 2) {

				if (!input[i].toLowerCase().equals(parameterType)) {
					throw new IllegalArgumentException(
							"Input Format: 1. -side x -side y -side \n 2. -angle x -angle y -angle z");
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Input Format: 1. -side x -side y -side \n 2. -angle x -angle y -angle z");
		}

		for (int i = 1, j = 0; i < 6; i += 2) {
			try {
				parameters[j++] = Integer.parseInt(input[i]);

			} catch (NumberFormatException e) {
				System.out
						.println("Argument must be an a positive and non zero int; maximum value:2147483647");
				throw e;

			}
		}
		return true;
	}

	public static boolean hasValidSides(float[] p) {
		return (p[0] > 0.0) && (p[1] > 0.0) && (p[2] > 0.0)
				&& (p[0] + p[1] > p[2]) && (p[1] + p[2] > p[0])
				&& (p[0] + p[2] > p[1]);
	}

	public static boolean hasValidAngles(float[] p) {
		return (p[0] + p[1] + p[02] == 180.00);
	}
}
