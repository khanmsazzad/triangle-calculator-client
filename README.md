# This is my README
This is the client side of RPC triangle calculator. 

Input should be either of the below format:

-side x -side y -side z
-angle x -angle y -angle z

Where x, y,z should be a number with maximum value of floating point. 

When multiple server is running, request is sent to multiple server in round robin manner. 


For coverage report:

You need maven 3. Run mvn clean install inside the project directory, you will find the report in target/site folder. 

